let tabs = document.querySelectorAll(".tabs-title");
let tabsContent = document.querySelector(".tabs-content").children;

for (let i = 0; i < tabsContent.length; i++) {
  tabsContent[i].classList.add("textContent");
}
let textContent = document.querySelectorAll(".textContent");

console.log(tabs);
console.log(tabsContent);

for (let i = 0; i < tabs.length; i++) {
  tabs[i].addEventListener("click", (e) => {
    for (let t = 0; t < tabs.length; t++) {
      tabs[t].classList.remove("active");
    }
    e.target.classList.add("active");

    for (let c = 0; c < textContent.length; c++) {
      textContent[c].classList.remove("activeContent");
      textContent[c].style.display = "none";
    }
    textContent[i].classList.add("activeContent");
    textContent[i].style.display = "block";
  });
}
